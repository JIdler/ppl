/*
* Parser for Descartes language
*
*
*
* Joey Idler
*/

import java.util.*;

public class Descartes {

	// Lexer object to get tokens
	private Lexer scan;

	// Root of recursive descent tree
	private StmtNode root;

	// Constructor
	public Descartes() {

		// Need to initialize Lexer

	}

	public final void buildTree() {

		// entry level non-terminal is prog
		root = prog();
		
		if(scan.getCurrSymb() != PERIOD.getID()) 
			new Error(1, "Error: Unexpected symbol " + 
						scan.getCurrSymb() + " found, expected PERIOD"); 

	}

	// non-terminal prog
	private final StmtNode prog() {

		// prog : stmt-list PERIOD
		StmtNode current = stmtList();

		return current;
	}

	// non-terminal stmt-list
	private final StmtNode stmtList() {

		// stmt-list : stmt stmt-tail
		StmtNode current = stmt();
		current = stmtTail(current);

		return current;
	}


	// stmt non-terminal
	private final StmtNode stmt() {

		// token number
		int lookahead = scan.getCurrSymb();
		StmtNode current = null;
		
		switch (lookahead) {

		case ID.getID(): 
			Becomes be = new Becomes();

			// Find assignment statement
			// stmt : assign-stmt
			be.assignment(scan);
			current = be;
			break;

		default:
			String msg = "Unrecognized statement: ";
			new Error(1, msg.concat(scan.getCurrName()));
		}

		return current;
	}

	// non-terminal stmt-tail
	private final StmtNode stmtTail(StmtNode current) {

		StmtNode nextStmt = null;;

		// stmt-tail : SEMICOLON stmt stmt-tail
		if(scan.getCurrSymb() == SEMICOLON.getID()) {

			scan.nextToken();
			nextStmt = stmt();
			current.setNext(nextStmt);
			current = stmtTail(nextStmt);
		} // else stmt-tail : 

		return current;		
	}


}
