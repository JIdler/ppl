/*
* for nodes containing operators +,-,*,/
*
*
* Joey Idler
*/

public class Operator extends ExprNode {

	protected ExprNode left, right;

	public Operator() {

		super();
		left = null;
	}

	public Operator(final int token) {

		super();
		super.kind = token;
		left = null;
	}

	public ExprNode getRight() {

		return right;
	}

	public ExprNode getLeft() {

		return left;
	}

	public void setRight(ExprNode expr) {

		right = expr;
	}

	public void setLeft(ExprNode expr) {

		if(left == null)
			left = expr;
		else
			left.setLeft(expr);
	}

}
