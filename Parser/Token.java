/*
* Token enums
*
*
*
* Joey Idler
*/


public enum Token {

	PERIOD(0),
	SEMICOLON(1),
	IF(2),
	THEN(3),
	ELSE(4),
	FI(5),
	LOOP(6),
	ID(7),
	COLON(8),
	REPEAT(9),
	BREAK(10),
	BECOMES(11),
	PRINT(12),
	READ(13),
	COMMA(14),
	OR(15),
	AND(16),
	LESS(17),
	LESS_EQUAL(18),
	EQUAL(19),
	GREATER_EQUAL(20),
	GREATER(21),
	NOT_EQUAL(22),
	PLUS(23),
	MINUS(24),
	MULT(25),
	DIV(26),
	OPEN(27),
	CLOSED(28),
	CONST(29);

	private final int id;

	Token(int id) {

		this.id = id;
	}	

	
	int getID() {

		return this.id;
	}
}
