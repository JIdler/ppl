/*
* for nodes representing numbers
*
*
*
* Joey Idler
*/

public class Factor {

	public Factor() {

	}

	public ExprNode parse(Lexer scan) {

		switch (scan.getCurrSymb()) {

		// factor : ID
		case Token.ID.getID():
			Id id = new Id(scan.getCurrName());
			scan.nextToken();
			return id;
		
		// factor : CONST
		case Token.NUMBER.getID():
			Number num = new Number(scan.getCurrValue());
			scan.nextToken();
			return num;

		// factor : ( expr )
		case Token.OPEN.getID():
			scan.nextToken();
			ExprNode expr = new ExprNode().parse(scan);
			scan.nextToken();
			return expr;

		default:
			String msg = "Error: unkown expression in Factor.parse(): ";
			new Error(3, msg.concat(scan.getCurrSymb()));
		}
		
		return null;
	}

}
