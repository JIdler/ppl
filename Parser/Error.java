/*
* Error class for Descartes parser
*
*
*
* Joey Idler
*/

public class Error {

	public Error(int num, String msg) {

		System.err.println("Error: number " + num + ": " + msg);
		System.exit(num);
	}

}
