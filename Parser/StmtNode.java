/*
* StmtNode
*
*
* Joey Idler
*/

public class StmtNode extends Node {

	// next statement
	private StmtNode next;

	// Default constructor
	public StmtNode() {

		super();
		next = null;
	}

	// accessor for next statement
	public StmtNode getNext() {

		returns next;
	}

	
	public void setNext(StmtNode node) {

		next = node;
	}

}
