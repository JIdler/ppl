/*
* for nodes representing numbers
*
*
* Joey Idler
*/

public class TermTail {

	public TermTail() {

	}

	public ExprNode parse(Lexer scan) {

		ExprNode sub, rhs;

		Term term = new Term();
		TermTail tt = new TermTail();

		int token = scan.getCurrSymb();

		if(token == Token.PLUS.getID() || token == Token.MINUS.getID()) {

			Operator op = new Operator(token);
			scan.nextToken();

			rhs = term.parse(scan);
			op.setRight(rhs);
			sub = tt.parse(scan);

			if(sub != null) {

				sub.setLeft(op);
				return sub;
			} else
				return op;
			
		}

		return null;
	}

}
