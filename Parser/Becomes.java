/*
* Becomes node
*
*
* Joey Idler
*/

public class Becomes extends StmtNode {

	private final IdNode lhs;
	private final ExprNode rhs;

	public Becomes() {

		super();
		super.kind = Token.BECOMES.getID();
	}

	public IdNode getLHS() {

		return lhs;
	}

	public ExprNode getRHS() {

		return rhs;
	}

	public void setRHS(ExprNode n) {

		rhs = n;
	}

	public void setLHS(IdNode n) {

		lhs = n;
	}

	// parses assign-stmt : ID BECOMES expr
	public void assignment(Lexer scan) {

		ExprNode expr = new ExprNode();

		String var = scan.getCurrName();

		lhs = new IdNode(var);

		scan.nextToken();

		if(scan.getCurrSymb() != Token.BECOMES.getID()) {

			String msg = "Error: Wrong token type found in Becomes.assignment: ";
			msg.concat(scan.getCurrName());
			new Error(2, msg);
		}

		scan.nextToken();
		rhs = expr.parse(scan);
	}
}
