/*
* Lexer class
*
*
*
* Joey Idler
*/

import java.util.*;

public class Lexer {

	private class TokenPair {

		// Token id value
		public final int token;

		// The string that the token id matches with
		public final String sequence;

		protected TokenPair(final int token, final String sequence) {

			this.token = token;
			this.sequence = sequence;
		}


	}


	private LinkedList<TokenPair> tokens;

	// empty constructor not allowed
	private Lexer() {};

	// Constructs lexer from file
	public Lexer(File file) {
		
		// Put code to read from file

	}

	// Returns token id thats at the front of the tokens linkedlist
	public final int getCurrSymb() {


	}

	// Returns token string thats at front of tokens linkedlist
	public final String getCurrName() {


	}

	// Returns token string at front of tokens linked list, parsed as a double
	public final Double getCurrValue() {

		
	}

	// Pops top TokenPair off linkedlist
	public final void nextToken() {


	}


}
