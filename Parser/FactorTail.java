/*
* for nodes representing numbers
*
*
* Joey Idler
*/

public class FactorTail {

	public FactorTail() {

	}

	public ExprNode parse(Lexer scan) {

		ExprNode sub, rhs;

		Factor factor = new Factor();
		FactorTail ft = new FactorTail();

		int token = scan.getCurrSymb();

		if(token == Token.MULT.getID() || token == Token.DIV.getID()) {

			Operator op = new Operator(token);
			scan.nextToken();

			rhs = factor.parse(scan);
			op.setRight(rhs);
			sub = ft.parse(scan);

			if(sub != null) {

				sub.setLeft(op);
				return sub;
			} else
				return op;
			
		}

		return null;
	}

}
