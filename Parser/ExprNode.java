/*
* Node for expressions
*
*
*
* Joey Idler
*/

public class ExprNode extends Node {

	public ExprNode() {

		super();
	}

	// What is this method for?
	public void setLeft(ExprNode n) {

		System.out.println("expr.setLeft");
	}

	public ExprNode parse(Lexer scan) {

		ExprNode sub, lhs;
		
		Term term = new Term();
		TermTail termTail = new TermTail();

		// get term and term-tail
		lhs = term.parse(scan);	
		sub = termTail.parse(scan);

		if(sub == null) {

			return lhs;
		} else {

			sub.setLeft(lhs);
			return sub;
		}
	}


}
