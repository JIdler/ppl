/*
* for nodes representing numbers
*
*
*
* Joey Idler
*/

public class Term {

	public Term() {

	}

	public ExprNode parse(Lexer scan) {

		ExprNode sub, lhs;

		Factor factor = new Factor();
		FactorTail ft = new FactorTail();

		lhs = factor.parse(scan);
		sub = ft.parse(scan);

		if(sub == null) {

			return lhs;
		} else {

			sub.setLeft(lhs);
			return sub;
		}
	}

}
