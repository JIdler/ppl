// Joey Idler
// GetToken.java

import java.util.regex.*;
import java.util.*;
import java.io.*;
import javafx.util.Pair;

public class GetToken {

	private List<String> tokens;

	private Map<String, Integer> t = new HashMap<String, Integer>();

	public GetToken(String filename) {

		this.tokenizer( filename );

		t.put("IF", 1);
		t.put("THEN", 2);
		t.put("ELSE", 3);
		t.put("FI", 4);
		t.put("LOOP", 5);
		t.put("BREAK", 6);
		t.put("READ", 7);
		t.put("REPEAT", 8);
		t.put("PRINT", 9);
		t.put("AND", 10);
		t.put("OR", 11);

		t.put(")",12);
		t.put("(",13);
		t.put("/",14);
		t.put("*",15);
		t.put("-",16);
		t.put("+",17);
		t.put("<>",18);
		t.put(">",19);
		t.put(">=",20);
		t.put("=",21);
		t.put("<=",22);
		t.put("<",23);
		t.put(":=",24);
		t.put(";",25);
		t.put("SPACE",26);
		t.put("EOLN",27);
		t.put("PERIOD",31);

	}

	private void tokenizer(String filename) {
		
		Scanner file = null;

		try {

			file = new Scanner(new BufferedReader(new FileReader(filename)));
		}
		catch( IOException e ) {

			System.out.println(e);
			System.exit(1);
		}

		this.tokens = new ArrayList<String>();
		Pattern pattern = 
			Pattern.compile("([a-zA-Z]\\w+)|((\\b[0-9]+)?\\.)?[0-9]+\\b|(\\s+)" + 
				"|(\\))|(\\()|(\\/)|(\\*)|(\\-)|(\\+)|(\\<>)|(\\>)" +
				"|(\\>=)|(\\=)|(\\<=)|(\\<)|(\\:=)|(\\;)|(\\.)|(\")");

		while(file.hasNext() && file != null) {

			String line = file.nextLine();
			line += "\n";

			Matcher matcher = pattern.matcher(line);

			while(matcher.find()) {

				tokens.add(matcher.group());
			}
		}
/*
		for(int i = 0; i < tokens.size(); i++) {

			System.out.println(tokens.get(i));
		}
*/
	}
	
	public Pair<String, Integer>  getToken() {
	
		if(tokens.isEmpty()) {
			return null;
		}

		String token = tokens.remove(0);

		if(token.equals(" ")) 
			token = "SPACE";
		else if(token.equals("\n"))
			token = "EOLN";
		else if(token.equals("."))
			token = "PERIOD";
		else if(token.equals(String.valueOf('"'))) {

			do {

				token = tokens.remove(0);
			} while(!token.equals(String.valueOf('"')));

			return new Pair("string",30);
		} 

		if(t.get(token) == null) {
	
			try {

				Float.parseFloat(token);
				return new Pair("number", 29);

			} catch(NumberFormatException e) {
				
				// This accepts "\t" as an identifier,
				// although I dont think it should based on instructions
				// Not sure how to remove "\t" though
				return new Pair("indentifier", 28);
			}

		} else {

			return new Pair(token, t.get(token));
		}
	
	}

	public static void main(String[] args) {

		GetToken test = new GetToken("token.dat");
		Pair tokenPair = test.getToken();
		
		while( tokenPair != null ) {
		
			System.out.println(tokenPair.getKey() + " " + tokenPair.getValue());

			tokenPair = test.getToken();
		}
	}

}


