/*
* GetToken method
*
* Joey Idler
*/

#include <iostream>
#include <string>
#include <unordered_map>
#include <utility>

 using namespace std;

 unordered_map<string, unsigned int> keywords = 
 { 
 	{"IF", 1},
   	{"THEN", 2},
	{"ELSE", 3},
	{"FI", 4},
	{"LOOP", 5},
	{"BREAK", 6},
	{"READ", 7},
	{"REPEAT", 8},
	{"PRINT", 9},
	{"AND", 10},
	{"OR", 11}
};

unordered_map<string, unsigned int> operators = 
{
	{')', 12},
	{'(', 13},
	{'/', 14},
	{'*', 15},
	{'-', 16},
	{'+', 17},
	{"<>", 18},
	{'>', 19},
	{">=", 20},
	{'=', 21},
	{"<=", 22},
	{'<', 23},
	{":=", 24},
	{';', 25},
	{' ', 26},
	{'\n', 27},

	{'.', 31}
};

ifstream file("token.dat");

pair<string, unsigned int> GetToken(void) {

	char c1 = file.get();

	if(c1.compare('>')||c1.compare('<')) {

		char c2 = c1 + file.peek();

		if(c2.compare("<>")||c2.compare(">=")||
				c2.compare("<=")) {

			c2 = c1 + file.get();

			return make_pair(c2, operators[c2]);
		}

		return make_pair(c1, operators[c1]);
	}

	char c2 = c1 + file.peek();

	if(c2.compare(":=")) {

		c2 = c1 + file.get();

		return make_pair(c2, operators[c2]);		
	}

	if(c1.compare('\n')) {

		return make_pair("EOLN",operators[c1]);
	}

	if(c1.compare(' ')) {
		
		return make_pair("SPACE", operators[c1]);
	}

	if(c1.compare('.')) {

		return make_pair("PERIOD",operators[c1]);
	}

	if(c1.compare('"')) {

		c2 = file.peek;

		while(!c2.compare('"')) {

			c1 += file.get();
		}

		c2 = file.get();

		return make_pair("string", 30);

	}

	if(!stoi(c1).fail()) {

		string number = file.get();
		char n = file.peek();

		while(!stoi(n).fail() || n.compare('.')) {

			number += file.get();
		}

		return make_pair("number", 29);
	}
	
	for(const auto &pair : operators) {

		if(c1.compare(pair.first)) {
		
			c1.get()
			return pair;
		}
	}

	string word = c1;

	c1 = file.peek();

	while(c1 != ')' && c1 != '(' && c1 != '/' && c1 != '*' c1 != '-' && c1 != '+' &&
		c1 != '<' && c1 != '>' && c1 != '=' && c1 != ':' && c1 != ';' && c1 != '' 
	


}

