/**
main.swift for Assignment 3
 
- Author:
 Joey Idler

- Version:
 0.0

 */


import Foundation

/// Prints the options that user can choose and their description
func userOptions() {
    
    print("Please choose one of the following options:\n")
    print("1 - Print the top 5 home run hitters and how many each hit for the current year")
    print("2 - Print the worst 5 batting averages and who they belong to, for the current year")
    print("3 - Print how many players finished the season with a different team from the one they began with")
    print("4 - Print which players averaged the most number of home runs per year over their career")
    print("5 - Print the highest paid players")
    print("6 - Print the players producing the least-costly home runs")
    print("7 - Print the 5 players that have been in the major leagues the longest")
    print("8 - Quit program\n")
}

/// A PlayerData object
let data = PlayerData()

// Print user options
userOptions()

repeat {

    print("Enter choice: ", terminator: "")
    
    /// Read user input from Standard Input
    let input = readLine(stripNewline: true)
    
    print("")
    
    // check if user gave an Integer as input
    // and if not then skip back to beginning of loop
    guard let userInput = Int(input!) else {
    
        print("Error: Incorrect input!")

        continue
    }

    // Quit program if user chose option 8
    // else print statistics according to user choice
    if userInput == 8 {
        
        break
    } else {
        
        data.printStats(userInput)
    }

} while true

// end main

