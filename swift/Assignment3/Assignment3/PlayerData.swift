/**
Allows user to manipulate baseball player data 
that is read in from a file.
 
- Author:
 Joey Idler
 
- Version:
 0.0
 
*/

import Foundation

public class PlayerData {
    
    /// Data for each player
    private var data: [[String:AnyObject]]
    
    /// Default Constructor
    init() {
        
        self.data = []
        
        self.loadData()
    }
    
    /**
     Loads data from file into `data`
     Default file is `hitter.data`
     
     - Parameters:
        - filename: Name of file containing data
    */
    private func loadData(fromFile filename: String = "hitter.data") {
        
        /// Read from `hitter.data`
        let file = FileReader(filename: filename)
        
        /// Temporary dictionary to hold data
        let dataBuff = [String:AnyObject](minimumCapacity: 24)
        var index = 0
        
        
        
        while file.hasNext() {
            
            let keyID = index % 24
            
            if keyID == 0 {
                
                self.data.append(dataBuff)
            }
            
            self.data[self.data.count - 1].updateValue(file.next(), forKey: keySwitcher( keyID ))
            
            index++
        }
        
    }
    
    /**
     Retrieves the key associated with a specific player data entry
     
     - Parameters:
        - fieldNumber: Int value pertaining to a case
            0. hitter's name,
            1. number of times at bat in 1986,
            2. number of hits in 1986,
            3. number of home runs in 1986,
            4. number of runs in 1986,
            5. number of runs batted in in 1986,
            6. number of walks in 1986,
            7. number of years in the major leagues,
            8. number of times at bat during his career,
            9. number of hits during his career,
            10. number of home runs during his career,
            11. number of runs during his career,
            12. number of runs batted in during his career,
            13. number of walks during his career,
            14. player's league at the end of 1986,
            15. player's division at the end of 1986,
            16. player's team at the end of 1986,
            17. player's position(s) in 1986,
            18. number of put outs in 1986,
            19. number of assists in 1986,
            20. number of errors in 1986,
            21. 1987 annual salary on opening day in thousands of dollars,
            22. player's league at the beginning of 1987,
            23. player's team at the beginning of 1987.
     
        - Returns: 
            - key associated with the `fieldNumber`
     
    */
    public func keySwitcher(fieldNumber: Int) -> String {
        
        switch fieldNumber {
            
        case 1:
            return "bats_86"
            
        case 2:
            return "hits_86"
            
        case 3:
            return "home_runs_86"
            
        case 4:
            return "runs_86"
            
        case 5:
            return "runs_batted_86"
            
        case 6:
            return "walks_86"
            
        case 7:
            return "years_in_ML"
            
        case 8:
            return "bats_during_career"
            
        case 9:
            return "hits_during_career"
            
        case 10:
            return "career_home_runs"
            
        case 11:
            return "career_runs"
            
        case 12:
            return "career_runs_batted"
            
        case 13:
            return "career_walks"
            
        case 14:
            return "end_league_86"
            
        case 15:
            return "end_division_86"
            
        case 16:
            return "end_team_86"
            
        case 17:
            return "positions_86"
            
        case 18:
            return "put_outs_86"
            
        case 19:
            return "assists_86"
            
        case 20:
            return "errors_86"
            
        case 21:
            return "salary_87"
            
        case 22:
            return "beginning_league_87"
            
        case 23:
            return "beginning_team_87"
            
        default:
            return "Name"
            
        }
        
    }
    
    /**
     Sorts `data` in descending order for a given key
     
     - Parameters:
        - key: key to sort `data` by
    */
    public func sortDescending(byKey key: String) {
        
        data.sortInPlace({ Float($0[key] as! String) > Float($1[key] as! String) })
        
    }
    
    /**
     Sorts `data` in ascending order for a given key
     
     - Parameters:
        - key: key to sort `data` by
    */
    public func sortAscending(byKey key: String) {
        
        data.sortInPlace({ Float($0[key] as! String) < Float($1[key] as! String) })
    }
    
    /**
     Builds an array of dictionaries that contains the first `num` 
     player entries in `data` and specific data for each of those players
     
     - Parameters: 
        - num: numbers of data entries to get from `data`
        - keys: data fields to get from each player
     
     - Returns:
        - array of dictionaries
    */
    public func getStats(num: Int, forFields keys: String...) -> [[String:AnyObject]] {
        
        /// Player stats
        var stats: [[String:AnyObject]] = []

        for index in 0...(num - 1) {
            
            stats.append([ "Name" : data[index]["Name"]! ])
            
            for key in keys {
            
                stats[index].updateValue( data[index][key]!, forKey: key )
            }
        }
        
        return stats
    }
    
    /**
     Calculates averages from data
     
     - Parameters:
        - num: key that will give the data
            to be used as the numerator when calculating the average
        - denom: key that will give the data
            to be used as the denominator when calculating the average
        - des: sort the averages in descending or ascending order? *defaults to descending*
     
     - Returns:
        An array of dictionaries containing each players name and
        the average calculated for each of them
    */
    public func getAverages
            (numerator num: String, denominator denom: String, descending des: Bool = true) -> [[String:AnyObject]] {
                
                /// Players averages
                var averages: [[String:AnyObject]] = []
                
                /// Container for players that have nil data entries
                var nilValues: [[String:AnyObject]] = []
                
                for player in data {
                    
                    let n = Float(player[num] as! String)
                    
                    let d = Float(player[denom] as! String)
                    
                    if n == nil {
                        
                        nilValues.append(["Name" : player["Name"]!])
                        
                        nilValues[nilValues.count - 1].updateValue(player[num]!, forKey: "average")
                        
                        continue
                    }

                    
                    if d == nil {
                        
                        nilValues.append(["Name" : player["Name"]!])
                        
                        nilValues[nilValues.count - 1].updateValue(player[denom]!, forKey: "average")
                        
                        continue
                    }

                    
                    averages.append(["Name" : player["Name"]!])
                    
                    let ave = n! / d!
                    
                    averages[averages.count - 1].updateValue(ave, forKey: "average")
                }
        
                if des {
                    
                    averages.sortInPlace({ ($0["average"] as! Float) > ($1["average"] as! Float) })
                }
                else {
                    
                    averages.sortInPlace({ ($0["average"] as! Float)  < ($1["average"] as! Float)  })
                }
                
                
                /// Place players that had nil entries onto end of averages array
                for player in nilValues {
                    
                    averages.append(player)
                }
                
                return averages
    }

    
    /// Performs calculations on the player data and prints the results
    public func printStats(option: Int) {
        
        switch option {
            
        // Print top 5 home run hitters and how many each hit for the current year
        case 1:
            sortDescending(byKey: "career_home_runs")
            
            let stats = getStats(5, forFields: keySwitcher(10), keySwitcher(3) )
            
            for player in stats {
                
                print("\(player["Name"]!) - Career Home Runs: \(player[keySwitcher(10)]!) ",terminator: "")
                print("Home Runs 1986: \(player[keySwitcher(3)]!)\n")
            }
            
        // Print worst 5 batting averages and who they belong to (hits / at bats) for the current year
        case 2:
            let worstAvs = getAverages(numerator: keySwitcher(2), denominator: keySwitcher(1), descending: false)
            
            for index in 0...4 {
                
                print(String(format: "\(worstAvs[index]["Name"]!) - Batting Average: %.3f\n",
                    (worstAvs[index]["average"] as! Float)))
            }
            
        // Print how many players finished the season with a different team from the one they began with
        case 3:
            var counter = 0
            
            for player in data {
                
                if (player[keySwitcher(16)] as! String) != (player[keySwitcher(23)] as! String) {
                    
                    print("\(player["Name"]!) - Beginning Team: \(player[keySwitcher(16)]!) ",terminator: "")
                    print("Ending Team: \(player[keySwitcher(23)]!)\n")
                    
                    counter++
                }
            }
            
            print("\(counter) players finished the season with a different team than they began with\n")
         
        // Print which players averaged the most number of home runs per year over their career
        // Prints top 5
        case 4:
            let avHRuns = getAverages(numerator: keySwitcher(10), denominator: keySwitcher(7))
            
            for index in 0...4 {
                
                print(String(format: "\(avHRuns[index]["Name"]!) - Average Home Runs Per Year: %.3f\n",
                    (avHRuns[index]["average"] as! Float)))
            }
            
        // Print the highest paid players
        // Prints top 5
        case 5:
            sortDescending(byKey: keySwitcher(21))
            
            for index in 0...4 {
                
                print("\(data[index]["Name"]!) - Salary: $\(data[index][keySwitcher(21)]!)\n")
            }
            
        // Print the players producing the least-costly home runs (salary / home runs)
        // Prints the 5 least-costly
        case 6:
            let ascendingHRCost = getAverages(numerator: keySwitcher(21), denominator: keySwitcher(3), descending: false)
            
            for index in 0...4 {
                
                print(String(format: "\(ascendingHRCost[index]["Name"]!) - Cost Per Home Run: $%.2f\n",
                    (ascendingHRCost[index]["average"] as! Float)))
            }
            
        // Print the 5 players that have been in the major leagues the longest
        case 7:
            sortDescending(byKey: keySwitcher(7))
            
            for index in 0...4 {
                
                print("\(data[index]["Name"]!) - Years in major leagues: \(data[index][keySwitcher(7)]!)\n")
            }
            
        // If user gives invalid input
        default:
            print("Oops somethings went wrong! Input another choice:")
        }

        
    }
    
    /// Simply prints all the data for each player in no specific order
    public func printData() {
        
        for player in data {
        
            for (dataField, playerData) in player {
        
                print("\(dataField): \(playerData)")
            }
        }
    }
    
}