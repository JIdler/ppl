/**
 This is a helper class that reads data from
 a text file and stores it in a container
 
 - Author:
    Joey Idler
 
 - Version:
    0.0
*/

import Foundation

public class FileReader{
    
    /// Path to file
    private let filePath: String
    
    /// Array to hold strings from file
    private var fileBuffer = [String]()
    
    /// Current index in the `fileBuffer`
    private var currentIndex = 0
    
    /**
     Constructs `FileReader` object using a given filename.
     Reads strings from file into Array.
     
     - Parameters:
        - filename: name of file to read from
    */
    init(filename: String) {
        
        /// Cocoa default FileManager
        let fileManager = NSFileManager.defaultManager()
        
        /// Path to the current directory
        let currentPath = fileManager.currentDirectoryPath
        
        // Set the path to the file
        self.filePath = (currentPath as NSString).stringByAppendingPathComponent( filename )
        
        // Put strings from file into array
        toArray()
    }
    
    /// Reads strings seperated by "\n" and "\t" from file into Array
    private func toArray() {
        
        /// Contents of file
        let fileContents: String
        
        do {
            
            fileContents = try String(contentsOfFile: filePath)
            
            let tempArray = fileContents.componentsSeparatedByString("\n")
            
            var fileArray = [String]()
            
            for lines in tempArray {
                
                fileArray += lines.componentsSeparatedByString("\t")
                
            }
            
            fileArray.removeLast()
            
            self.fileBuffer = fileArray
            
            
        } catch let error as NSError {
            
            print("Error: \(error.domain) in intoArray function")
            
            exit(1)
        }
        
        
    }
    
    /// Helper function to check if there are still strings 
    /// left in the array when using `next()`
    public func hasNext() -> Bool {
        
        return currentIndex < fileBuffer.count ? true : false
    }
    
    /// Helper function to get each string from the array
    public func next() -> String {
        
        assert(self.hasNext(), "Error: Array out of bounds in function next()")
        
        let temp = fileBuffer[currentIndex]
        
        currentIndex++
        
        return temp
    }
    
    /// Helper function to return to beginning of array
    public func rewind() {
        
        currentIndex = 0
    }
    
}
