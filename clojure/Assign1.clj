; Joey Idler

(def temp '(a b c d e))

(def evTemp '(a b c d e f g h))

(def ll '((:a :b) (:b :a) (:p :q :r :s)))

(defn delete-at [items loc]
	(if (= loc 0) (rest items)
		(cons (first items) (delete-at (rest items) (- loc 1)))))

(defn invert [items]
	(if (= (count items) 2) (list (second items) (first items))
		(let [x (second items)] (cons x (cons (first items) (invert (rest (rest items))))))))	

(defn put-as-first [i lList]
	(if (= (count lList) 1) (list (cons i (first lList)))
		(cons (cons i (first lList)) (put-as-first i (rest lList)))))

;Unfortunately I couldnt figure out how to make place work properly and I didnt get to the 
;next two functions 

(defn place [i items]
	(if (= (first items) i) 
		(if (= (count items) 2) (list (second items) (first items))
			(list items))
		(if (= (count items) 1) (place i (cons i items))
			(list (place i (cons i items)) (cons (first items) (place i (rest items)))))))

