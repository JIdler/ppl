; Joey Idler

(def family  '( (:colin  nil  nil 1921) (:deidre  nil  nil 1926) (:arthur  nil  nil 1919)
	(:kate  nil  nil 1923) (:frank  nil  nil 1929) 
	(:linda  nil  nil 1929) (:suzanne  :colin  :deidre 1947)
	(:bruce  :arthur :kate 1943) (:charles  :arthur  :kate 1945) 
	(:david  :arthur  :kate 1947) (:ellen  :arthur  :kate 1952)
	(:george  :frank  :linda 1952) (:hillary  :frank  :linda 1954) 
	(:andre  nil  nil 1948) (:tamara  :bruce  :suzanne 1970)
	(:vincent :bruce :suzanne 1973) (:wanda  nil  nil 1980) 
	(:ivan :george :ellen 1976) (:zach :bruce :ellen 1979) 
	(:julie :george :ellen 1981) (:marie :george :ellen 1988) 
	(:nigel :andre :hillary 1984) (:fredrick nil :tamara 1987) 
	(:joshua  :ivan  :wanda 2001) (:quentin  nil  nil 1979) 
	(:robert  :quentin  :julie 2001) (:olivia  :nigel  :marie 2009) (:peter  :nigel  :marie 2010)))



(defn oldest [lst]

	(let [year (first (rest (rest (rest (first lst)))))
		person (first lst)
		nPerson (second lst)]

	(if (= (count lst) 2) 
		(if (< year  
			(first (rest (rest (rest nPerson))))) 
				(list year (first person))
			;else is nPerson older?
				(if (> year 
					(first (rest (rest (rest nPerson))))) 
				(list (first (rest (rest (rest nPerson)))) (first nPerson))
				;else they must be same age
					(list year 
						(first person) (first nPerson))))
		;else more than 2 people in list
		(if (< year 
			(first (oldest (rest lst)))) (list year 
				(first person))
			;else person is older?
				(if (> year 
					(first (oldest (rest lst)))) 
					(oldest (rest lst))
					;else same age?
						(concat (oldest (rest lst)) (list (first person))))))))

(defn father [lst name]
	(if-not (keyword? name) (father lst (keyword name))
		(if (= (first (first lst)) name) ;else for first if
			(if (= (second (first lst)) nil) nil
				 (second (first lst)))
			(father (rest lst) name))))
			



(defn mother [lst name]
	(if-not (keyword? name) (mother lst (keyword name))
		(if (= (first (first lst)) name) ;else for first if
			(if (= (second (rest (first lst))) nil) nil
				(second (rest (first lst))))
			(mother (rest lst) name))))
			


(defn parents [lst name]
	(let [f (father lst name) m (mother lst name)]
		(if (and (= f nil) (= m nil)) nil
			(cond
				(= f nil) (list m)
				(= m nil) (list f)
				:else (list f m)))))




(defn children [lst name]
	(if (= (count lst) 1)
		(if (or (= (father lst (first (first lst))) name) (= (mother lst (first (first lst))) name))
			(list (first (first lst)))
				nil) ;else return nothing
		;else list is bigger than 1
		(if (or (= (father lst (first (first lst))) name) (= (mother lst (first (first lst))) name))
			(cons (first (first lst)) (children (rest lst) name))
				;else keep looking
				(children (rest lst) name))))


(defn siblings 
	([lst name] 
		(let [p (parents lst name)]
			(if (= p nil) nil
				(siblings lst p name))))
	([lst par name]

		(let [lPar (parents lst (first (first lst)))]
			
			(if (= (count lst) 1) 
				(if (= (count par) 2) 
					(cond
						(= lPar nil) nil
						(= (first (first lst)) name) nil
				 		(= (first par) (first lPar)) (list (first (first lst)))
						(= (second par) (second lPar)) (list (first (first lst)))
						(= (first par) (second lPar)) (list (first (first lst)))
						(= (second par) (first lPar)) (list (first (first lst)))
						:else nil)
					(cond
						(= lPar nil) nil
						(= (first (first lst)) name) nil
						(= (first par) (first lPar)) (list (first (first lst)))
						(= (first par) (second lPar)) (list (first (first lst)))
						:else nil))

			;else lst > 1
				(if (= (count par) 2)
					(cond
						(= lPar nil) (siblings (rest lst) par name)
						(= (first (first lst)) name) 
							(siblings (rest lst) par name)
						(= (first par) (first lPar)) 
								(cons (first (first lst)) 
									(siblings (rest lst) par name))
						(= (second par) (second lPar)) 
							 	(cons (first (first lst)) 
									(siblings (rest lst) par name))
						(= (first par) (second lPar))
								(cons (first (first lst)) 
									(siblings (rest lst) par name))
						(= (second par) (first lPar))
								(cons (first (first lst))
									(siblings (rest lst) par name))
						:else (siblings (rest lst) par name))
					
					(cond
						(= lPar nil) (siblings (rest lst) par name)
						(= (first (first lst)) name) 
							(siblings (rest lst) par name)
						(= (first par) (first lPar)) 
								(cons (first (first lst))
									(siblings (rest lst) par name))
						(= (first par) (second lPar)) 
								(cons (first (first lst))
									(siblings (rest lst) par name))
						:else (siblings (rest lst) par name)))))))
						
						
						
				
	
	
(defn grandparents [lst name]	
	(let [p (parents lst name)]
		(if (= p nil) nil 
			(if (= (count p) 1) (parents lst (first p))
				(if (empty? (concat (parents lst (first p)) (parents lst (second p)))) nil
					(concat (parents lst (first p)) (parents lst (second p))))))))
				

(defn cousins [lst name]
	(let [p (parents lst name)]
		(if (= p nil) nil
			(if (= (count p) 1)
				(let [sibs (siblings lst (first p))]
					(if (= sibs nil) nil
						(loop [cuz nil s sibs]
							(if (= (first s) nil)
								cuz
								(recur (concat (children lst (first s)) cuz)
									(rest s))))))
				(let [sibs (concat (siblings lst (first p)) (siblings lst (second p)))]
					(if (= sibs nil) nil
						(loop [cuz nil s sibs]
							(if (= (first s) nil)
								cuz
								(recur (concat (children lst (first s)) cuz)
									(rest s))))))))))

	



;Need to fix so doesnt include multiples
(defn ancestors [lst name]
	(let [p (parents lst name)]
		(if (= p nil) nil
			(if (= (count p) 1) (concat p (ancestors lst (first p)))
				(concat p (concat (ancestors lst (first p)) (ancestors lst (second p))))))))





